import 'dart:async';

import 'package:flutter/material.dart';
import 'package:practicando_app/screens/login.dart';
import 'package:practicando_app/screens/app_tabs.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
     @override
     Widget build(BuildContext context) {
       return new MaterialApp(
         title: 'Flutter Demo',         
         home: new MyApp_(title: 'Flutter Demo Home Page'),
         routes: <String, WidgetBuilder> {
           "login" : (BuildContext context) => new Login(),
         }
       );
     }
}
class MyApp_ extends StatefulWidget {
  final String title;
  MyApp_({Key key, this.title}) : super(key: key);     
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp_> {
  SharedPreferences sharedPreferences;
  @override
  void initState() {
    checkLoginStatus();
    super.initState();    
  }

    checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
        Navigator.of(context).pushNamed("login");
    }
  }

  @override
  Widget build(BuildContext context) {    
    return MaterialApp(
      home: AppTabs()
    );
  }
}
