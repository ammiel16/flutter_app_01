import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:practicando_app/screens/login.dart';

class Logout extends StatefulWidget{
  @override
  _LogoutState createState() => _LogoutState();
}

class _LogoutState extends State<Logout> {
  SharedPreferences sharedPreferences;
  bool _isLoading=false;
  
  logout() async{
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.clear();
           //sharedPreferences.commit();
    sharedPreferences.remove("token");
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => Login()), (Route<dynamic> route) => false);   
    /*Map data = {
    'token': sharedPreferences.getString("token")
    };
    var jsonResponse = null;
    var response = await http.post("http://192.168.1.27:8080/api/logout",
                  body: data);
      print("prueba12345");
     print(response.statusCode);     
    if(response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
          setState(() {
            _isLoading = false;
          });
          sharedPreferences.clear();
          sharedPreferences.commit();
          sharedPreferences.remove("token");
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => Login()), (Route<dynamic> route) => false);
        }
    }else if(response.statusCode == 500){
      jsonResponse = json.decode(response.body);
      if(jsonResponse != null) {
       setState(() {
      _isLoading = false; 
       });
       sharedPreferences.clear();
       sharedPreferences.commit();
       sharedPreferences.remove("token");
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => Login()), (Route<dynamic> route) => false);
      }
    }
    else {
    setState(() {
    _isLoading = false; 
    });
   }*/
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: RaisedButton(
        padding:EdgeInsets.all(15.0),
        color: Colors.redAccent,
        child: _isLoading? Center(child: CircularProgressIndicator()):
        Text("Salir", style: TextStyle(color:Colors.white, fontSize: 15.0)),
        onPressed: (){
          setState(() {
            _isLoading=true;
          });           
          logout();
        }),
    );
  }
}