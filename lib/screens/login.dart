import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:practicando_app/screens/app_tabs.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class Login extends StatefulWidget {
  @override
  _MyLoginState createState() => _MyLoginState();
}

class _MyLoginState extends State<Login> {
  bool _isLoading = false;
  String textMsgValidate="";
  
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  signIn(String email, pass) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map data = {
    'email': email,
    'password': pass
    };
    var jsonResponse = null;
    var response = await http.post("http://192.168.1.27:8080/api/login",
                  body: data);
    if(response.statusCode == 200) {
    jsonResponse = json.decode(response.body);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    if(jsonResponse != null) {
        setState(() {
        _isLoading = false;
        });
        sharedPreferences.setString("token", jsonResponse['token']);
         Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => AppTabs()), (Route<dynamic> route) => false);
        }
    }
    else {
    setState(() {
    _isLoading = false;  
    textMsgValidate= "Usuario y/o contraseña no validos";  
    });
    print(response.body);
   }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(      
      appBar: AppBar(
        title: Text("Login", style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        brightness: Brightness.light,
        iconTheme: IconThemeData(
            color: Colors.black87
        ),
        ),
        drawer: Drawer(
          child: Container()// Populate the Drawer in the next step.
      ),  
      backgroundColor: Colors.lightBlueAccent,    
      body: _isLoading ? Center(child: CircularProgressIndicator()) :
        Form(        
        child:Container(
              padding: EdgeInsets.all(55.0), 
              margin: EdgeInsets.all(30.0),
              height: 500,  
              decoration: BoxDecoration(
               borderRadius: BorderRadius.circular(30.0),
                color: Colors.white,
               boxShadow: [
                  BoxShadow(
                    blurRadius: 2.0,
                    color: Colors.white
                  ),                  
                ]
              ),           
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,           
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[ 
              Center(
                child: Text("Bienvenido", style: TextStyle(fontSize: 30.0),)
              ),  
              SizedBox(
                height: 40,
              ),        
            TextFormField(
              controller: emailController,
              decoration:  InputDecoration(
                hintText: 'Enter your email',
                border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(20.0),
                          borderSide: new BorderSide(
                          ),
                ),
              ),
            ),
            SizedBox(height: 20,),
            TextFormField(
              controller: passwordController,
              decoration:  InputDecoration(
                hintText: 'Enter your password',
                border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(20.0),
                          borderSide: new BorderSide(
                          ),
                ),
              ),
            ),
            SizedBox(height: 30),
            Text(textMsgValidate, style: TextStyle(color: Colors.redAccent)),          
            SizedBox(
              width: double.infinity,            
              child:RaisedButton(                
                elevation: 10.0,
                padding:EdgeInsets.all(20.0),
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.blueAccent)
                ),
                color: Colors.blueAccent,
                child: Text("Entrar", style: TextStyle(color:Colors.white, fontSize: 15.0),),
                 onPressed: () {
                    setState(() {
                      _isLoading=true;
                    });
                    signIn(emailController.text, passwordController.text);
                })
                )
          ]))
            ),
    );
  }
}
