import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:practicando_app/model/locations.dart' as locations;

class MyMap extends StatefulWidget{
   @override
  _MyMap createState()=>_MyMap();
}
class _MyMap extends State<MyMap>{
  GoogleMapController mapController;
  String buscarDireccion;
  final Map<String, Marker> _markers={};
  CameraPosition cameraPosition= CameraPosition(
    target: LatLng(0,0),
    zoom: 2.0
  );
  Future<void> _onMapCreated(GoogleMapController controller) async{
    final googleOffices= await locations.getGoogleOffices();
    setState(() {
      _markers.clear();
      for(final office in googleOffices.offices){
        final marker= Marker(
          markerId: MarkerId(office.name),
          position: LatLng(office.lat, office.lng),
          infoWindow: InfoWindow(
            title: office.name,
            snippet: office.address
          )
        );
        _markers[office.name]=marker;
      }
    });
  }
  /*void _onMapCreated(GoogleMapController controller){
    mapController =controller;
  }*/
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mapa en flutter'),
        centerTitle: true,
      ),
     body: Stack(
       children: <Widget>[
         GoogleMap(
           mapType: MapType.normal,
           onMapCreated: _onMapCreated,
           initialCameraPosition: cameraPosition,
           markers: _markers.values.toSet()  
          )
       ],
     )
    );
  }
}
